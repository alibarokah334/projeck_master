import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import React, {Component} from 'react';

const Tombol = ({label, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style = {style.mama}>
      <Text>{label}</Text>
    </TouchableOpacity>
  );
};

export class Home extends Component{
  render () {
    return (
      <View>
        <Tombol label={'Part Satu'} />
        <Tombol label={'Part Dua'} />
        <Tombol label={'Part Tiga'} />
      </View>
    );
  }
}

export default Home;

const style = StyleSheet.create ({
  mama: {
    margin: 10,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'gray',
  },
});