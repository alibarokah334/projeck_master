import {View,Text} from 'react-native';
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {ParSatu,Home} from '../pages/index';
const Navigasi = createNativeStackNavigator ();

const Route = () => {
    return (
        <Navigasi.Navigasi>
            <Navigasi.Screen name="Home" component={Home } />
            <Navigasi.Screen name="ParSatu" component={ParSatu } />

        </Navigasi.Navigasi>

    );
};

export default Route;